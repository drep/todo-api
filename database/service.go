package database

import (
	"database/sql"

	// mysql driver used by sqlx
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/drep/todo-api/config"
)

type client interface {
	Get(dest interface{}, query string, args ...interface{}) error
	Exec(query string, args ...interface{}) (sql.Result, error)
	Ping() error
	PrepareNamed(query string) (*sqlx.NamedStmt, error)
	Select(dest interface{}, query string, args ...interface{}) error
}

type Database struct {
	Client client
}

func Dial(conf config.Config) (*Database, error) {
	// does not actually open a DB connection
	client, err := sqlx.Open("mysql", conf.DBConnStr)
	if err != nil {
		return nil, err
	}
	client.SetMaxOpenConns(10)
	client.SetMaxIdleConns(10)
	return &Database{Client: client}, nil
}

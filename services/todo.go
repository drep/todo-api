package services

import (
	"time"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/drep/todo-api/database"
)

type Todo struct {
	ID        int64          `json:"id" db:"id"`
	Title     string         `json:"title"`
	Complete  bool           `json:"complete"`
	Completed mysql.NullTime `json:"completed"`
	Archived  bool           `json:"archived"`
	Created   time.Time      `json:"created"`
	Updated   time.Time      `json:"json"`
}

const getAllQuery = `
SELECT
	id,
	title,
	complete,
	completed,
	archived,
	created,
	updated
FROM todo
ORDER BY created desc
`

const getOneQuery = `
SELECT
	id,
	title,
	complete,
	completed,
	archived,
	created,
	updated
FROM todo
WHERE
	id=?
`

const markCompleteQuery = `
UPDATE todo
SET
	complete=1,
	completed=NOW()
WHERE
	complete=0 AND
	id=?
`

const deleteQuery = `
	DELETE FROM todo
	WHERE
		id=?
`

func GetAllTodos(db *database.Database) ([]Todo, error) {
	todos := make([]Todo, 0)
	err := db.Client.Select(&todos, getAllQuery)
	return todos, err
}

func GetOneTodo(db *database.Database, id string) (Todo, error) {
	todo := Todo{}
	err := db.Client.Get(&todo, getOneQuery, id)
	return todo, err
}

func CompleteTodo(db *database.Database, id string) (bool, error) {
	result, err := db.Client.Exec(markCompleteQuery, id)
	if err != nil {
		return false, err
	}
	rowsAffected, err := result.RowsAffected()
	return rowsAffected == 1, err
}

func DeleteTodo(db *database.Database, id string) (bool, error) {
	result, err := db.Client.Exec(deleteQuery, id)
	if err != nil {
		return false, err
	}
	rowsAffected, err := result.RowsAffected()
	return rowsAffected == 1, err
}

FROM scratch
COPY todo ./app
ENTRYPOINT ["/app"]
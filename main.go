package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/rs/cors"

	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/drep/todo-api/config"
	"gitlab.com/drep/todo-api/database"
	"gitlab.com/drep/todo-api/routes"
)

// injected by build (see Makefile)
var (
	GitCommit string
	GitBranch string
	Version   string
)

func main() {
	addr := os.Getenv("LISTEN_ADDR")
	if addr == "" {
		addr = "localhost:5000"
	}

	conf := config.Config{
		DBConnStr: os.Getenv("MYSQL"),
	}

	if conf.DBConnStr == "" {
		log.Fatal("Database connection string required, please use env var 'MYSQL'")
	}

	db, err := database.Dial(conf)
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not dial the database: %v\n", err))
	}

	// fail fast
	err = db.Client.Ping()
	if err != nil {
		log.Fatal(fmt.Sprintf("Could not ping the database: %v\n", err))
	}

	buildInfo := config.BuildInfo{
		GitCommit: GitCommit,
		GitBranch: GitBranch,
		Version:   Version,
	}

	router := routes.Generate(buildInfo, db)
	handler := cors.Default().Handler(router)
	log.Printf("server started: %s\n", addr)
	log.Fatal(http.ListenAndServe(addr, handler))
}

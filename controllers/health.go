package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/drep/todo-api/config"
	"gitlab.com/drep/todo-api/database"
)

type HealthCheckResponse struct {
	StartTime time.Time `json:"startTime"`
	config.BuildInfo
}

func Health(buildInfo config.BuildInfo, db *database.Database) http.HandlerFunc {
	healthCheckResponse := HealthCheckResponse{
		StartTime: time.Now(),
		BuildInfo: buildInfo,
	}
	json, err := json.Marshal(healthCheckResponse)
	if err != nil {
		log.Fatal(err)
	}

	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		err := db.Client.Ping()
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		res.Header().Set("Content-Type", "application/json")
		res.Write(json)
	})
}

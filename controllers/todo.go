package controllers

import (
	"fmt"
	"log"
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/drep/todo-api/database"
	"gitlab.com/drep/todo-api/services"
)

func responseJson(res http.ResponseWriter, obj interface{}) {
	res.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(res).Encode(obj)
	if err != nil {
		http.Error(res, err.Error(), http.StatusInternalServerError)
		return
	}
}

func GetAllTodos(db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		todos, err := services.GetAllTodos(db)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		responseJson(res, todos)
	})
}

func GetTodo(db *database.Database) httprouter.Handle {
	return httprouter.Handle(func(res http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		id := ps.ByName("id")
		todo, err := services.GetOneTodo(db, id)
		if err != nil {
			if err == sql.ErrNoRows {
				http.Error(res, fmt.Sprintf("todo not found for id %s", id), http.StatusNotFound)
				return
			}
			log.Printf("err %s", err.Error())
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		responseJson(res, todo)
	})
}

func CompleteTodo(db *database.Database) httprouter.Handle {
	return httprouter.Handle(func(res http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		id := ps.ByName("id")
		success, err := services.CompleteTodo(db, id)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		if !success {
			http.Error(res, "todo not found or was already marked completed", http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusOK)
		res.Write([]byte("OK"))
	})
}

/*func CreateTodo(db *database.Database) http.HandlerFunc {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		todos.CreateTodo()
	})
}*/

func DeleteTodo(db *database.Database) httprouter.Handle {
	return httprouter.Handle(func(res http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		id := ps.ByName("id")
		success, err := services.DeleteTodo(db, id)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		if !success {
			http.Error(res, "todo not found", http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusOK)
		res.Write([]byte("OK"))
	})
}

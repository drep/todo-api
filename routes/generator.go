package routes

import (
	"github.com/julienschmidt/httprouter"

	"gitlab.com/drep/todo-api/config"
	"gitlab.com/drep/todo-api/controllers"
	"gitlab.com/drep/todo-api/database"
)

func Generate(buildInfo config.BuildInfo, db *database.Database) *httprouter.Router {
	router := httprouter.New()
	router.HandlerFunc("GET", "/health", controllers.Health(buildInfo, db))
	router.GET("/todos/:id", controllers.GetTodo(db))
	router.DELETE("/todos/:id", controllers.DeleteTodo(db))
	router.POST("/todos/:id/complete", controllers.CompleteTodo(db))
	router.HandlerFunc("GET", "/todos", controllers.GetAllTodos(db))
	//router.HandlerFunc("POST", "/todos", controllers.CreateTodo(db))
	return router
}

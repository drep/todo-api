package config

type BuildInfo struct {
	GitCommit string `json:"gitCommit"`
	GitBranch string `json:"gitBranch"`
	Version   string `json:"version"`
}

type Config struct {
	DBConnStr string `json:"-"`
}

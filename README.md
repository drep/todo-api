# Todo API

## Prereqs

Install [golang](https://golang.org/doc/install) and have a mysql-compatible database available. On Mac you can run the following:
```
brew install golang mariadb
```

Rather than installing mariadb, you can instead use Docker: [instructions found below](#running-with-docker).

## Get Project
```
mkdir -p ~/go/src/gitlab.com/drep
cd ~/go/src/gitlab.com/drep
git clone git@gitlab.com:drep/todo-api.git
```

### Setup Database

Run `mysql -uroot` and create a database and user.
```
create database personal;
create user 'bill'@'localhost' identified by 'mypass';
grant all privileges on personal.* to 'bill'@'localhost';
grant create view on *.* to 'bill'@'localhost';
exit;
```

Setup the schema:
```
mysql -u bill -p personal < ~/go/src/gitlab.com/drep/todo-api/schema.sql
```

Create a file named `.env` with the database connection string, including your credentials:
```
echo "MYSQL=bill:mypass@/personal?parseTime=true" > ~/go/src/gitlab.com/drep/todo-api/.env
```

## Run App
```
GO111MODULE=on
make run
```

## Running with Docker

This is not necessary if you've installed MariaDB locally.

### MySQL/MariaDB

**Warning**: [docker on a Mac host behaves differently from linux](https://docs.docker.com/docker-for-mac/networking/#use-cases-and-workarounds).

So, the provided Makefile assumes that you've created a user-defined bridge network named "my-net", which allows the containers to talk to one another.
```
docker network create my-net
```

Then you can start a dockerized mysql:
```
make docker-run-mysql
```

Then connect and apply schema.sql 
```
mysql -h 127.0.0.1 -P 3307 -u bill --password=mypass personal < ~/go/src/gitlab.com/drep/todo-api/schema.sql
```

Also run the app in a docker container:
```
MYSQL="bill:mypass@tcp(mysql0)/personal?parseTime=true" make docker-run
```
^ **Warning**: this is janky: for some reason the container name resolves with the wrong port (3306).

## API Endpoints
You can hit the healthcheck endpoint, which verifies the database connection:
```
curl localhost:5000/health
```

Get all todos from the database:
```
curl localhost:5000/todos
```

Get a single todo from the database, retrieving by ID:
```
curl localhost:5000/todos/1
```

Complete a todo, by ID:
```
curl -X POST localhost:5000/todos/2/complete
```

Delete a todo, by ID:
```
curl -X DELETE localhost:5000/todos/2
```
GIT_COMMIT := $(shell git rev-list -1 HEAD)
GIT_BRANCH := $(if $(shell echo $$BRANCH_NAME),$(shell echo $$BRANCH_NAME),$(shell git rev-parse --abbrev-ref HEAD))
VERSION := $(if $(shell echo $$VERSION),$(shell echo $$VERSION),"localdev-unset")

build: 
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
	go build \
		-ldflags "-X main.GitCommit=${GIT_COMMIT} -X main.GitBranch=${GIT_BRANCH} -X main.Version=${VERSION}" \
		-o todo main.go

run:
	go run \
		-ldflags "-X main.GitCommit=${GIT_COMMIT} -X main.GitBranch=${GIT_BRANCH} -X main.Version=${VERSION}" \
		main.go

docker: build
	docker build -t drep/todo:latest .

docker-run: docker
	docker run -p 5000:5000 --name todo0 --network my-net --rm -e LISTEN_ADDR=0.0.0.0:5000 -e MYSQL=$$MYSQL drep/todo:latest

docker-run-mysql:
	# todo: should persist data
	docker run -p 3307:3307 --name mysql0 --network my-net --rm -e MYSQL_TCP_PORT=3307 -e MYSQL_ROOT_PASSWORD=password -e MYSQL_USER=bill -e MYSQL_PASSWORD=mypass -e MYSQL_DATABASE=personal mysql:5.7

.PHONY: build run docker docker-run